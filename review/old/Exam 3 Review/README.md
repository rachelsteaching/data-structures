# Exam 3 Review

## Rules

For the exam, it is **Solo Effort**. Students must use the lab computers
in the classroom while taking the exam; personal computers are not allowed.

Students **may** use their **textbook** (physical book, or ebook),
and they **may** use their notes from the **note templates** (electronic version only)
during the exam.

Students will be monitored from the instructor computer while the exam is in progress.

Students **may** use Visual Studio as-needed, but in general it will not be needed.

## Topics

1. Trees
2. Binary Search Trees
3. Heaps Basics
4. Dictionaries
5. Balanced Search Trees Basics
6. Review: Stacks and Queues
7. Review: Linked Lists
8.
