# Final Exam Review

## Rules

For the exam, it is **Solo Effort**. Students must use the lab computers
in the classroom while taking the exam; personal computers are not allowed.

Students **may** use their **textbook** (physical book, or ebook),
and they **may** use their notes from the **note templates** (electronic version only)
during the exam.

Students will be monitored from the instructor computer while the exam is in progress.

Students **may** use Visual Studio as-needed, but in general it will not be needed.

## Question Breakdown

| Question Description                                  | Weight        |
| ----------------------------------------------------- | ------------- |
| Trees: Terminology                                    | 5% |
| Trees: Terminology                                    | 5% | 
| Trees: In-order traversal                             | 5% | 
| Trees: Pre-order traversal                            | 5% | 
| Trees: Post-order traversal                           | 5% | 
| Trees: Identify properties                            | 3% | 
| Trees: Identify properties                            | 4% | 
| Trees: Building a binary search tree                  | 3% | 
| Trees: Building a binary search tree                  | 4% | 
| Trees: Family identification                          | 3% | 
| Dictionaries: Linear probing                          | 2% | 
| Dictionaries: Quadratic probing                       | 2% | 
| Heaps: Build a heap array                             | 2% | 
| BST: Balance factor and height                        | 3% | 
| Stacks: Stack operations                              | 4% | 
| Stacks: Stack operations                              | 4% | 
| Stacks: Stack operations                              | 3% | 
| Queues: Queue operations                              | 4% | 
| Queues: Queue operations                              | 4% | 
| Queues: Queue operations                              | 3% | 
| Efficiency: Common algorithms                         | 9% | 
| Efficiency: Identify growth rate                      | 6% | 
| Efficiency: Identify growth rate                      | 6% | 
| Efficiency: Identify growth rate                      | 6% | 

## Topics

### Trees

* Basic tree terminology
* Traversing binary trees (in-order, pre-order, post-order)
* Building a binary search tree from a list of Push operations

### Dictionaries

* Given a hash table state, hash function, & collision operation, be able to figure out the index for a new item based on its key.

### Heaps

* Being able to take a tree and build out the array (review lab)

### Balanced Search Trees

* Identifying node heights & balance factors (review lab)

### Stacks and Queues

* Basic stack/queue operations and diagramming (review quizzes)

### Algorithm Efficiency

* Knowing the growth rate of data structure functions (access/insert/search)
    * Unsorted array
    * Linked lists
    * Binary search tree

* Being able to see code and identify the efficiency
