# Exam 2 Review

## Rules

For the exam, it is **Solo Effort**. Students must use the lab computers
in the classroom while taking the exam; personal computers are not allowed.

Students **may** use their **textbook** (physical book, or ebook),
and they **may** use their notes from the **note templates** (electronic version only)
during the exam.

Students will be monitored from the instructor computer while the exam is in progress.

Students **may** use Visual Studio as-needed, but in general it will not be needed.

## Topics

1. Algorithm efficiency (5 questions)
2. Searching/sorting algorithms (2 questions)
3. Stack and Queue states (6 questions)
4. Stack and Queue code (5 questions)

### Algorithm efficiency

Several functions will be presented and you have to identify whether the growth function is **Constant O(1)**, **Linear O(n)**, or **Quadratic O(n<sup>2</sup>)**.

### Searching/sorting algorithms

Be able to describe the efficiency of the sorting algorithms we covered, as well as a linear search vs. a smarter search algorithm. Why is it easier to do a smart search on sorted data?

### Stack/Queue states

These will be questions like on the quizzes where a list of commands are given (Push, Pop, etc.) and you have to fill in the diagram for the Stack or Queue's state after the commands are executed.

### Stack/Queue code

You will be asked to identify whether a function belongs to a Stack or a Queue. The code will be given and you have to mark which is belongs to. This includes **array-based** and **link-based** implementations of **Push**, **Pop**, and **Get (Top/Front)** functions.
