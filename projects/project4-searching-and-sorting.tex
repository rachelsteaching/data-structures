\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Data Structures with C++}
\newcommand{\laTitle}       {Searching and Sorting}
\newcounter{question}

\toggletrue{answerkey}      %\togglefalse{answerkey}

\setcounter{chapter}{3}
\renewcommand{\chaptername}{Project}

\input{../BASE-2-HEADER}

    \tableofcontents

\newpage

    \chapter{\laTitle}
    
    \input{../BASE-3-INSTRUCTIONS-PROJECT}

    For this project, you will be investigating different sorting algorithms
    for part 1, and implementing various sorting algorithms in part 2.

    There's a project in the starter files that contains the textbook's
    implementations of the sort functions (\texttt{Example-Sorting}),
    as well as the program you'll be completing in part 2 in the
    \texttt{Sorting-Disease} folder.

    \newpage
    \section{Part 1: Investigating Sorting Algorithms}
    
    For this program, you will implement the sort functions in the \texttt{SortFunctions.cpp} file.
    You might want to check out this YouTube video,
    ``Visualization and Comparison of Sorting Algorithms'', here: ~\\
    \texttt{https://www.youtube.com/watch?v=ZZuD6iUe3Pc}

    \begin{center}
        \includegraphics[width=14cm]{images/lab11-visualization.png}
    \end{center}

    To get acquainted with how these sorting algorithms work, first you
    are going to do a \textit{trace}\footnote{stepping through the code as if you're the computer executing the instructions.}
    through small lists to be sorted to see how everything changes over time.

    This might be easiest to work on in a text document, but you can also do it on paper
    and scan in your work. Make sure your work is organized, clear, and legible.

    ~\\
    You can use the text file \texttt{Project 4 Part 1.odt} if you want.
    It is a LibreOffice Writer file, but you can also open it in MS Word.
    See this file for an example of the Selection Sort trace.

    \paragraph{Input array:} ~\\
    For each sorting algorithm, use \texttt{\{"C", "A", "T"\}}
    for your inputs.

    \newpage %---------------------------------------------------------- Selection Sort -%
    \subsection{Selection Sort}

    Wikipedia Entry: \texttt{https://en.wikipedia.org/wiki/Selection\_sort}

    \paragraph{Efficiency:}

    \begin{center}
        \begin{tabular}{c c c}
            Best & Average & Worst \\ \hline
            $O(n^{2})$ & $O(n^{2})$ & $O(n^{2})$
        \end{tabular}
    \end{center}

    \paragraph{Helper function: findIndexOfLargest} ~\\
   
\begin{lstlisting}[style=smallcode]
/** HELPER FUNCTION:
 Finds the largest item in an array.
 @pre  The size of the array is >= 1.
 @post  The arguments are unchanged.
 @param theArray  The given array.
 @param size  The number of elements in theArray.
 @return  The index of the largest entry in the array. */
template<class ItemType>
int findIndexOfLargest(const ItemType theArray[],
                        int size)
{
    // Index of largest entry found so far
    int indexSoFar = 0; 
    for (int currentIndex = 1; currentIndex < size;
            currentIndex++)
    {
      // At this point, theArray[indexSoFar] >= all
      // entries in theArray[0..currentIndex - 1]
      if (theArray[currentIndex] > theArray[indexSoFar])
         indexSoFar = currentIndex;
    }  // end for
    return indexSoFar; // Index of largest entry
}  // end findIndexOfLargest
\end{lstlisting}
\footnotesize From Data Abstraction \& Problem Solving with C++, 7th ed, Carrano and Henry \normalsize

    \newpage
    \paragraph{Sort function: selectionSort} ~\\
    
\begin{lstlisting}[style=smallcode]
/** Sorts the items in an array into ascending order.
 @pre  None.
 @post  The array is sorted into ascending order;
        the size of the array is unchanged.
 @param theArray  The array to sort.
 @param n  The size of theArray. */
template<class ItemType>
void selectionSort(ItemType theArray[], int n)
{
   // last = index of the last item in the subarray
   // of items yet to be sorted;
   // largest = index of the largest item found
   for (int last = n - 1; last >= 1; last--)
   {
    // At this point, theArray[last+1..n-1] is sorted,
    // and its entries are greater than those in
    // theArray[0..last].
    // Select the largest entry in theArray[0..last]
    int largest = findIndexOfLargest(theArray, last+1);
      
    // Swap the largest entry, theArray[largest], with
    // theArray[last]
    std::swap(theArray[largest], theArray[last]);
   }  // end for
}  // end selectionSort
\end{lstlisting}

\footnotesize From Data Abstraction \& Problem Solving with C++, 7th ed, Carrano and Henry \normalsize
    
    \newpage %---------------------------------------------------------- Bubble Sort -%
    \subsection{Bubble Sort}

    Wikipedia Entry: \texttt{https://en.wikipedia.org/wiki/Bubble\_sort}

    \paragraph{Efficiency:}

    \begin{center}
        \begin{tabular}{c c c}
            Best & Average & Worst \\ \hline
            $O(n)$ & $O(n^{2})$ & $O(n^{2})$
        \end{tabular}
    \end{center}

    \paragraph{Sort function: bubbleSort} ~\\

\begin{lstlisting}[style=smallcode]
/** Sorts the items in an array into ascending order.
 @pre  None.
 @post  theArray is sorted into ascending order;
        n is unchanged.
 @param theArray  The given array.
 @param n  The size of theArray. */
template<class ItemType>
void bubbleSort(ItemType theArray[], int n)
{
  bool sorted = false; // False when swaps occur
  int pass = 1;
  while (!sorted && (pass < n))
  {
    // At this point, theArray[n+1-pass..n-1]
    // is sorted and all of its entries are
    // > the entries in theArray[0..n-pass]
    sorted = true; // Assume sorted
    for (int index = 0; index < n - pass; index++)
    {
      // At this point, all entries in
      // theArray[0..index-1] are <= theArray[index]
      int nextIndex = index + 1;
      if (theArray[index] > theArray[nextIndex])
      {
        // Exchange entries
        std::swap(theArray[index], theArray[nextIndex]);
        sorted = false; // Signal exchange
      } // end if
    }  // end for
    // Assertion: theArray[0..n-pass-1] < theArray[n-pass]

    pass++;
  }  // end while
}  // end bubbleSort
\end{lstlisting}
\footnotesize From Data Abstraction \& Problem Solving with C++, 7th ed, Carrano and Henry \normalsize


    \newpage %---------------------------------------------------------- Insertion Sort -%
    \subsection{Insertion Sort}

    Wikipedia Entry: \texttt{https://en.wikipedia.org/wiki/Insertion\_sort}

    \paragraph{Efficiency:}

    \begin{center}
        \begin{tabular}{c c c}
            Best & Average & Worst \\ \hline
            $O(n)$ & $O(n^{2})$ & $O(n^{2})$
        \end{tabular}
    \end{center}

    \paragraph{Sort function: insertionSort} ~\\

\begin{lstlisting}[style=smallcode]
/** Sorts the items in an array into ascending order.
 @pre  None.
 @post  theArray is sorted into ascending order;
        n is unchanged.
 @param theArray  The given array.
 @param n  The size of theArray. */
template<class ItemType>
void insertionSort(ItemType theArray[], int n)
{
  // unsorted = first index of the unsorted region,
  // loc = index of insertion in the sorted region,
  // nextItem = next item in the unsorted region.
  // Initially, sorted region is theArray[0],
  // unsorted region is theArray[1..n-1].
  // In general, sorted region is
  // theArray[0..unsorted-1],
  // unsorted region theArray[unsorted..n-1]
  for (int unsorted = 1; unsorted < n; unsorted++)
  {
    // At this point, theArray[0..unsorted-1] is sorted.
    // Find the right position (loc) in
    // theArray[0..unsorted] for theArray[unsorted],
    // which is the first entry in the unsorted region;
    // shift, if necessary, to make room
    ItemType nextItem = theArray[unsorted];
    int loc = unsorted;
    while ((loc > 0) && (theArray[loc - 1] > nextItem))
    {
      // Shift theArray[loc - 1] to the right
      theArray[loc] = theArray[loc - 1];
      loc--;
    }  // end while
  
    // At this point, theArray[loc] is where
    // nextItem belongs
    theArray[loc] = nextItem;   // Insert nextItem into
                                // sorted region
  }  // end for
}  // end insertionSort
\end{lstlisting}
\footnotesize From Data Abstraction \& Problem Solving with C++, 7th ed, Carrano and Henry \normalsize

    \newpage %---------------------------------------------------------- Merge Sort -%
    \subsection{Merge Sort}

    Wikipedia Entry: \texttt{https://en.wikipedia.org/wiki/Merge\_sort}

    \paragraph{Efficiency:}

    \begin{center}
        \begin{tabular}{c c c}
            Best & Average & Worst \\ \hline
            $O(n log(n))$ & $O(n log(n))$ & $O(n log(n))$
        \end{tabular}
    \end{center}

    \paragraph{Helper function: merge} ~\\

\begin{lstlisting}[style=smallcode]
/** HELPER FUNCTION
    Merges two sorted array segments
    theArray[first..mid] and theArray[mid+1..last] into one sorted array.
    
 @pre  first <= mid <= last. The subarrays theArray[first..mid] and theArray[mid+1..last] are each sorted in increasing order.
 @post  theArray[first..last] is sorted.
 @param theArray  The given array.
 @param first  The index of the beginning of the first segment in theArray.
 @param mid  The index of the end of the first segment in theArray; mid + 1 marks the beginning of the second segment.
 @param last  The index of the last element in the second segment in theArray.
 @note  This function merges the two subarrays into a temporary array and copies the result into the original array theArray. */
template<class ItemType>
void merge(ItemType theArray[],
    int first, int mid, int last)
{
   ItemType tempArray[MAX_SIZE];  // Temporary array
   
   // Initialize the local indices to indicate the subarrays
   int first1 = first;   // Beginning of first subarray
   int last1 = mid;      // End of first subarray
   int first2 = mid + 1; // Beginning of second subarray
   int last2 = last;     // End of second subarray
   
   // While both subarrays are not empty, copy the
   // smaller item into the temporary array


   // Next available location in tempArray
   int index = first1;  
   while ((first1 <= last1) && (first2 <= last2))
   {
      // At this point, tempArray[first..index-1] is in order
      if (theArray[first1] <= theArray[first2])
      {
         tempArray[index] = theArray[first1];
         first1++;
      }
      else
      {
         tempArray[index] = theArray[first2];
         first2++;
      }  // end if
      index++;
   }  // end while
   
   // Finish off the first subarray, if necessary
   while (first1 <= last1)
   {
      // At this point, tempArray[first..index-1] is in order
      tempArray[index] = theArray[first1];
      first1++;
      index++;
   }  // end while
   
   // Finish off the second subarray, if necessary
   while (first2 <= last2)
   {
      // At this point, tempArray[first..index-1] is in order
      tempArray[index] = theArray[first2];
      first2++;
      index++;
   }  // end for
   
   // Copy the result back into the original array
   for (index = first; index <= last; index++)
      theArray[index] = tempArray[index];
}  // end merge
\end{lstlisting}
\footnotesize From Data Abstraction \& Problem Solving with C++, 7th ed, Carrano and Henry \normalsize

    \newpage
    \paragraph{Sort function: mergeSort} ~\\
    
\begin{lstlisting}[style=smallcode]
/** Sorts the items in an array into ascending order.
 @pre  theArray[first..last] is an array.
 @post  theArray[first..last] is sorted in ascending order.
 @param theArray  The given array.
 @param first  The index of the first element to consider in theArray.
 @param last  The index of the last element to consider in theArray. */
template<class ItemType>
void mergeSort(ItemType theArray[], int first, int last)
{
   if (first < last)
   {
      // Sort each half
      
      // Index of midpoint
      int mid = first + (last - first) / 2; 
      
      // Sort left half theArray[first..mid]
      mergeSort(theArray, first, mid);
      
      // Sort right half theArray[mid+1..last]
      mergeSort(theArray, mid + 1, last);
      
      // Merge the two halves
      merge(theArray, first, mid, last);
   }  // end if
}  // end mergeSort
\end{lstlisting}
\footnotesize From Data Abstraction \& Problem Solving with C++, 7th ed, Carrano and Henry \normalsize











    \newpage
    \section{Part 2: Implementing Sorting Algorithms}




    For this project, you will be implementing different sorting algorithms
    and testing their efficiency with a timer.

    \subsection{Input files}

    There are multiple input files provided, because the original data
    file was large and can take a long time to sort. So, instead, you
    can choose one of the different versions of the file to test with.

    \begin{itemize}
        \item   \texttt{100\_US\_Chronic\_Disease\_Indicators.csv}
        \item   \texttt{1000\_US\_Chronic\_Disease\_Indicators.csv}
        \item   \texttt{10000\_US\_Chronic\_Disease\_Indicators.csv}
        \item   \texttt{523487\_US\_Chronic\_Disease\_Indicators.csv}
    \end{itemize}

    These files are Open Data from
    https://healthdata.gov/dataset/us-chronic-disease-indicators-cdi

    \newpage
    \subsection{Data files}

    The data files are a CSV (Comma Separated Value) format, so they
    can be opened up as plaintext files, or in a spreadsheet editor.

    The data looks like this:

    \begin{center}
        \includegraphics[width=14cm]{images/lab11-sorting.png}
    \end{center}

    or, in a text editor, like this:
    
    \begin{center}
        \includegraphics[width=\textwidth]{images/lab11-data2.png}
    \end{center}

    The data headers are:

    \begin{enumerate}
        \item   YearStart
        \item   YearEnd
        \item   LocationAbbr
        \item   LocationDesc
        \item   DataSource
        \item   Topic
        \item   Question
        \item   Response ~\\
                ... etc ...
    \end{enumerate}
    
    \section{The program}

    When you run this program, it will ask which file to load up...

\begin{verbatim}
-------------------------------------------------------------
 | U.S. Chronic Disease Indicators |
 -----------------------------------

Which file do you want to load?
 1.	100_US_Chronic_Disease_Indicators.csv
 2.	1000_US_Chronic_Disease_Indicators.csv
 3.	10000_US_Chronic_Disease_Indicators.csv
 4.	523487_US_Chronic_Disease_Indicators.csv

 >> 
\end{verbatim}

    Then you'll select which sort to use...

\begin{verbatim}
Which sort do you want to use?
 1.	Selection Sort
 2.	Merge Sort

 >> 
\end{verbatim}

    And then which column to sort the data on...

\begin{verbatim}
Which column do you want to sort on?
 1.	YearStart
 2.	YearEnd
 3.	LocationAbbr
 4.	LocationDesc
 5.	Topic
 6.	Question

 >> 
\end{verbatim}

    \newpage
    It will load the file and sort it, timing both operations:

\begin{verbatim}
BEGIN:         Loading data from file,
                "100_US_Chronic_Disease_Indicators.csv"...
COMPLETED:     In 3 milliseconds

99 items loaded
BEGIN:         Sorting data with Selection Sort...
COMPLETED:     In 2 milliseconds


Writing list out to "output.txt"...
\end{verbatim}

    The main point here is to see how long it takes to use each search,
    using different file sizes.
    

    \newpage
    \subsection{Program code}

    For this program, you will choose \textbf{two sorting algorithms}
    (besides Selection Sort, which is provided as an example)
    to implement. You'll need to add the function headers and bodies
    to \texttt{SortFunctions.hpp} and \texttt{SortFunctions.cpp},
    and update main to call the functions.
    
    \subsubsection{Writing Sorting Algorithms}

    In \texttt{SortFunctions.hpp}, you will need to add function headers
    for your sorting algorithms. The SelectionSort function is provided
    for you as an example:

\begin{lstlisting}[style=code]
#ifndef _SORT_FUNCTIONS_HPP
#define _SORT_FUNCTIONS_HPP

#include <iostream>
#include <vector>
using namespace std;

#include "DataEntry.hpp"

void SelectionSort( vector<DataEntry>& data, const string& onKey );

#endif
\end{lstlisting}

    Your sort function should work with vectors of \texttt{DataEntry} types.
    The DataEntry type is the data loaded from those CSV files.

    ~\\

    The function bodies will go in \texttt{SortFunctions.cpp},
    and you'll have to modify it slightly based on what field we're sorting by.
    For example, look at a vanilla Selection Sort 
    and compare it to the version in the \texttt{SortFunctions.cpp} file.
    The main thing is that, when comparing two elements of the array,
    we're looking at a specific field instead of the entire element.

    \footnotesize

    \paragraph{Generic version:} \texttt{if ( data[i] < data[iMin] )}

    \paragraph{Special version:} \texttt{if ( data[i].fields[onKey] < data[iMin].fields[onKey] )}

    \normalsize

    \newpage
    \subsubsection{Updating main()}

    In main, there will be a couple of places to update.
    First, there's a vector that stores the list of sort names,
    which is used on the main menu. It looks like this:

\begin{lstlisting}[style=code]
vector<string> sorts = {
    "Selection Sort",
    // PUT YOUR SORT FUNCTION NAMES HERE
    "Sort1",
    "Sort2"
};
\end{lstlisting}

    Add your sort function names here.

    ~\\

    Then, you will have to make sure to call your sort function
    if it's chosen by the user. That code looks like this:

\begin{lstlisting}[style=code]
// Sort the data
if ( sortChoice == 1 )
{
    SelectionSort( data, columns[ sortOnChoice - 1 ] );
}
else if ( sortChoice == 2 )
{
    /* TODO: Call basic sorting algorithm here */
    // Your Sort 1
}
else if ( sortChoice == 3 )
{
    /* TODO: Call faster sorting algorithm here */
    // Your Sort 2
}
\end{lstlisting}

    Add in the function calls for your sorting algorithms in chocie 2 and 3.



    \section{Turn in}

    Once completed, turn in the two parts. Part 1 should be a text file
    or written on paper and scanned in. ~\\

    For Part 2, upload the following files: \texttt{main.cpp},
    \texttt{SortFunctions.hpp}, and \texttt{SortFunctions.cpp}
    
\input{../BASE-4-FOOT}
