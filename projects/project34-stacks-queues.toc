\contentsline {chapter}{\numberline {3}Stacks and Queues}{2}
\contentsline {section}{\numberline {3.1}Input files}{3}
\contentsline {section}{\numberline {3.2}Output files}{3}
\contentsline {section}{\numberline {3.3}Coding}{5}
\contentsline {subsection}{\numberline {3.3.1}Queue}{5}
\contentsline {subsection}{\numberline {3.3.2}Stack}{6}
\contentsline {subsection}{\numberline {3.3.3}FactoryProgram}{7}
\contentsline {subsubsection}{FactoryProgram constructor (done)}{7}
\contentsline {subsubsection}{FactoryProgram destructor (done)}{7}
\contentsline {subsubsection}{FactoryProgram::Run() (done)}{7}
\contentsline {subsubsection}{void FactoryProgram::LoadIngredients()}{8}
\contentsline {subsubsection}{void FactoryProgram::AssembleBurgers()}{8}
\contentsline {subsubsection}{bool FactoryProgram::IsIngredientAcceptable( const Ingredient\& ingredient ) const}{9}
\contentsline {subsubsection}{void FactoryProgram::CheckBurgerQuality()}{9}
\contentsline {subsubsection}{void FactoryProgram::CheckBurgerCompletion()}{10}
\contentsline {subsubsection}{void FactoryProgram::PutIngredientOnBurger( Burger\& burger, const Ingredient\& ingredient ) (done)}{11}
\contentsline {subsubsection}{void FactoryProgram::DeliverBurgers()}{11}
\contentsline {section}{\numberline {3.4}Turn in}{12}
