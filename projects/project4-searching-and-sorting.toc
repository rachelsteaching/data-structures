\contentsline {chapter}{\numberline {4}Searching and Sorting}{2}
\contentsline {section}{\numberline {4.1}Part 1: Investigating Sorting Algorithms}{3}
\contentsline {paragraph}{Input array:}{3}
\contentsline {subsection}{\numberline {4.1.1}Selection Sort}{4}
\contentsline {paragraph}{Efficiency:}{4}
\contentsline {paragraph}{Helper function: findIndexOfLargest}{4}
\contentsline {paragraph}{Sort function: selectionSort}{5}
\contentsline {subsection}{\numberline {4.1.2}Bubble Sort}{6}
\contentsline {paragraph}{Efficiency:}{6}
\contentsline {paragraph}{Sort function: bubbleSort}{6}
\contentsline {subsection}{\numberline {4.1.3}Insertion Sort}{7}
\contentsline {paragraph}{Efficiency:}{7}
\contentsline {paragraph}{Sort function: insertionSort}{7}
\contentsline {subsection}{\numberline {4.1.4}Merge Sort}{9}
\contentsline {paragraph}{Efficiency:}{9}
\contentsline {paragraph}{Helper function: merge}{9}
\contentsline {paragraph}{Sort function: mergeSort}{11}
\contentsline {section}{\numberline {4.2}Part 2: Implementing Sorting Algorithms}{12}
\contentsline {subsection}{\numberline {4.2.1}Input files}{12}
\contentsline {subsection}{\numberline {4.2.2}Data files}{13}
\contentsline {section}{\numberline {4.3}The program}{14}
\contentsline {subsection}{\numberline {4.3.1}Program code}{16}
\contentsline {subsubsection}{Writing Sorting Algorithms}{16}
\contentsline {paragraph}{Generic version:}{16}
\contentsline {paragraph}{Special version:}{16}
\contentsline {subsubsection}{Updating main()}{17}
\contentsline {section}{\numberline {4.4}Turn in}{17}
