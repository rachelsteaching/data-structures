\contentsline {chapter}{\numberline {11}Searching and Sorting}{2}
\contentsline {section}{\numberline {11.1}Input files}{2}
\contentsline {section}{\numberline {11.2}Data files}{3}
\contentsline {section}{\numberline {11.3}The program}{4}
\contentsline {section}{\numberline {11.4}Program code}{6}
\contentsline {subsection}{\numberline {11.4.1}Bubble Sort}{7}
\contentsline {paragraph}{Efficiency:}{7}
\contentsline {paragraph}{Pseudocode:}{7}
\contentsline {subsection}{\numberline {11.4.2}Insertion Sort}{8}
\contentsline {paragraph}{Pseudocode:}{8}
\contentsline {subsection}{\numberline {11.4.3}Selection Sort}{9}
\contentsline {subsection}{\numberline {11.4.4}Quick Sort}{10}
\contentsline {subsection}{\numberline {11.4.5}Merge Sort}{11}
\contentsline {section}{\numberline {11.5}Turn in}{11}
